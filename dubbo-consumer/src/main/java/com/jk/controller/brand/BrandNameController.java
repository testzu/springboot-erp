package com.jk.controller.brand;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.Brand;
import com.jk.model.BrandName;
import com.jk.model.ClassTree;
import com.jk.service.brand.BrandNameService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("BrandName")
public class BrandNameController {

    @Reference
    private BrandNameService brandNameService;


   @RequestMapping("toShow")
  public String toShow(){
       return "/view/bootShow";
   }

   @RequestMapping("test")
    public void test(){
       brandNameService.gettest();
   }

   @RequestMapping("selectBrandNameList")
    @ResponseBody
    public HashMap<String ,Object> selectBrandNameList(Integer pageSize, Integer start,BrandName BrandName){
       String brandState = BrandName.getBrandState();
       String text = BrandName.getText();
       if(brandState.equals("全部")) {
           BrandName.setBrandState(null);
       }

       if(BrandName.getBrandName().equals("全部")){
           BrandName.setBrandName(null);
       }
       if(BrandName.getText().equals("全部")){
           BrandName.setText(null);
       }else{
           String[] split = text.split("/");
           String name=split[1];
           BrandName.setText(name);
           String textone=split[0];
           BrandName.setBrand(textone);
       }
       HashMap<String ,Object> mapList=brandNameService.selectBrandNameList(pageSize,start,BrandName);
       return mapList;
   }


  @RequestMapping("queryBrandNameType")
    @ResponseBody
   public List<Brand> queryBrandNameType(){
      List<Brand> BrandList=brandNameService.queryBrandNameType();

      return BrandList;
  }
  @RequestMapping("queryBrandNameTree")
    @ResponseBody
   public List<Brand> queryBrandNameTree(){
      List<Brand> BrandList=brandNameService.queryBrandNameTree();

      return BrandList;
  }

  @RequestMapping("queryAdd")
    public String queryAdd(){

       return "/view/addBrandName";
  }


  @RequestMapping("queryBrandList")
   @ResponseBody
    public List<Brand> queryBrandList(){
      List<Brand> list= brandNameService.queryBrandList();
      return list;
  }
@RequestMapping("queryClassTree")
  @ResponseBody
    public List<ClassTree> queryClassTree(){
    List<ClassTree> list=brandNameService.queryClassTree();
    return list;
}
@RequestMapping("saveBrandName")
    public String saveBrandName(BrandName BrandName){
   // System.out.println("11111["+BrandName+"]");
    brandNameService.saveBrandName(BrandName);
    return "/view/bootShow";
}





}
