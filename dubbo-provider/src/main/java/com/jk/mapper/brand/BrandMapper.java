package com.jk.mapper.brand;

import com.jk.model.brand.Brand;


import java.util.List;

public interface BrandMapper {

    List<Brand> queryBrandList(Brand branch);


    void addBrand(Brand branch);

    void updateBrand(Brand branch);

    void deleteBrandById(String[] ids);

    Brand toUpdateBrand(Brand brand);
}
