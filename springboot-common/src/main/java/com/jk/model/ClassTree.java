package com.jk.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ClassTree implements Serializable {
    private Integer id;

    private String text;

    private String href;

    private Boolean selectable;

    private List<ClassTree> nodes;

    private Integer pid;
}
