package com.jk.service.tree;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.tree.BootMapper;
import com.jk.model.tree.BootTree;
import com.jk.model.tree.ClassTree;
import com.jk.model.tree.TreeBean;
import com.jk.model.tree.Ztree;
import netscape.security.Privilege;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass =BootService.class )
@Component
public class BootServiceImpl implements BootService {
    @Autowired
    private BootMapper bootMapper;


    @Override
    public List<ClassTree> getClassTree() {
        Integer pid = -1;
        List<ClassTree> classTree = getClassTreeNodes(pid);
        return classTree;
    }
    private List<ClassTree> getClassTreeNodes(Integer pid) {
        List<ClassTree> classTree = bootMapper.getClassTreeNodes(pid);
        for (ClassTree classTreeBean : classTree) {
            Integer id2 = classTreeBean.getId();
            List<ClassTree> classTreeNode = getClassTreeNodes(id2);
            if (classTreeNode == null || classTreeNode.size() <= 0) {
                classTreeBean.setSelectable(true);
            } else {
                classTreeBean.setSelectable(false);
            }
            classTreeBean.setNodes(classTreeNode);
        }
        return classTree;
    }

    @Override
    public List<Ztree> queryZtreeList() {
        return bootMapper.queryZtreeList();
    }

    @Override
    public List<TreeBean> geteasyuiTree() {
        Integer pid=-1;
        List<TreeBean> treeNode = treeNode(pid);
        return treeNode;
    }


    @Override
    public void savePidClass(Ztree ztree) {
       Ztree ztList = new Ztree();
       if(ztList.getPid()==null){
           ztList.setPid(-1);
       }
        bootMapper.savePidClass(ztree);
    }

    @Override
    public void addEasyuiTree(TreeBean treeBean) {
        TreeBean tree = new TreeBean();
        if(tree.getPid()==null){
            tree.setPid(-1);
        }
        bootMapper.addEasyuiTree(treeBean);
    }

    @Override
    public List<TreeBean> getTreeBeanUtils() {
        return bootMapper.getTreeBeanUtils();
    }

    @Override
    public void saveTreeBean(TreeBean treeBean) {
        bootMapper.saveTreeBean(treeBean);
    }

    @Override
    public void updateTreeBean(TreeBean treeBean) {
        bootMapper.updateTreeBean(treeBean);
    }

    @Override
    public TreeBean queryById(TreeBean treeBean) {
        return bootMapper.queryById(treeBean);
    }

    @Override
    public void delByIds(Integer id) {
        bootMapper.delByIds(id);
    }

    private List<TreeBean> treeNode(Integer pid) {
        List<TreeBean> treeList = bootMapper.queryTreeNode(pid);
        for (TreeBean treeBean : treeList) {
            Integer id2 = treeBean.getId();
            List<TreeBean> treeNode = treeNode(id2);
			if(treeNode.size() >0){
				//如果有子节点，那么父节点处于关闭状态
                treeBean.setState("closed");
			} else {
				//没有子节点，那么父节点处于打开状态
                treeBean.setState("open");
			}
            treeBean.setChildren(treeNode);
        }
        return treeList;
    }

    /**
     * index下拉树
     * @return
     */
    @Override
    public List<BootTree> getBootTree() {
        int pid = -1;
        List<BootTree> bootTrees = getBootTreeNodes(pid);
        return bootTrees;
    }

    private List<BootTree> getBootTreeNodes(Integer pid) {
        List<BootTree> bootTrees = bootMapper.getBootTreeNodes(pid);
        for (BootTree bootTreeBean : bootTrees) {
            Integer id2 = bootTreeBean.getId();
            List<BootTree> bootTreeNode = getBootTreeNodes(id2);
            if (bootTreeNode == null || bootTreeNode.size() <= 0) {
                bootTreeBean.setSelectable(true);
            }else {
                bootTreeBean.setSelectable(false);
                bootTreeBean.setNodes(bootTreeNode);
            }
        }
        return bootTrees;
    }


}
