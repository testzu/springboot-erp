package com.jk.service.shiro;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.shiro.UserMapper;
import com.jk.model.User;
import com.jk.model.tree.BootTree;
import com.jk.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Service(interfaceClass = UserService.class)
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Override
    public User selectbyname(String userName) {
        return null;
    }

    @Override
    public User queryname(String userName) {
        return userMapper.queryname(userName);
    }

    @Override
    public List<String> puerybyid(Integer userId) {
        return null;
    }

    @Override
    public List<BootTree> querytree() {
        return null;
    }
}
