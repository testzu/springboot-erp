package com.jk.service.brand;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.brand.BrandNameMapper;
import com.jk.model.Brand;
import com.jk.model.BrandName;

import com.jk.model.ClassTree;
import com.jk.service.brand.BrandNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Service(interfaceClass = BrandNameService.class)
@Component
public class BrandNameServiceImpl implements BrandNameService {
    @Autowired
    private BrandNameMapper brandNameMapper;



    @Override
    public void gettest() {
        System.out.println("=================================================================");
    }

    @Override
    public HashMap<String, Object> selectBrandNameList(Integer pageSize, Integer start, BrandName brandName) {
        Integer count=brandNameMapper.selectCount(brandName);
       List<BrandName>  list= brandNameMapper.selectBrandNameList(pageSize,start,brandName);
        HashMap<String, Object> mapList=new HashMap<String, Object>();
        mapList.put("rows",list);
        mapList.put("total",count);
        return mapList;
    }

    @Override
    public List<Brand> queryBrandNameType() {
        List<Brand> list=brandNameMapper.queryBrandNameType();
        return list;
    }

    @Override
    public List<Brand> queryBrandNameTree() {
        List<Brand> list=  brandNameMapper.queryBrandNameTree();

        return list;
    }

    @Override
    public List<Brand> queryBrandList() {
        List<Brand> list=brandNameMapper.queryBrandList();
        return list;
    }

    @Override
    public List<ClassTree> queryClassTree() {
        List<ClassTree> list =brandNameMapper.queryClassTree();
        return list;
    }

    @Override
    public void saveBrandName(BrandName BrandName) {
        Integer brandNo = BrandName.getBrandNo();
        if(brandNo==null){
            Integer   mxa=brandNameMapper.selectMax();
            Integer mx = mxa+1;
            BrandName.setBrandNo(mx);
        }
        BrandName.setBrandState("在架");
        System.out.println("2222["+BrandName+"]");
        brandNameMapper.saveBrandName(BrandName);

    }


}
