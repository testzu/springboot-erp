package com.jk.service.zy;

import com.jk.model.zy.Spec;

import java.util.HashMap;

public interface SepcService {

    HashMap<String, Object> querysepc(Integer pageSize, Integer start, Spec spec);

    void addspec(Spec spec);

    Spec queryspecById(Integer id);

    void updatespec(Spec spec);

    void deletespecbyid(Integer id);
}
