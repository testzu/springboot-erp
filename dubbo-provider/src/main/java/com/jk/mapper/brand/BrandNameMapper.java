package com.jk.mapper.brand;


import com.jk.model.Brand;
import com.jk.model.BrandName;
import com.jk.model.ClassTree;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface BrandNameMapper {


    int selectCount();


    List<BrandName> selectBrandNameList(@Param("pageSize") Integer pageSize, @Param("ta") Integer start, @Param("brand") BrandName brandName);

    Integer selectCount(@Param("brand") BrandName brandName);

    List<Brand> queryBrandNameType();

    List<Brand> queryBrandNameTree();

    List<Brand> queryBrandList();

    List<ClassTree> queryClassTree();

    Integer selectMax();

    void saveBrandName(BrandName brandName);
}
