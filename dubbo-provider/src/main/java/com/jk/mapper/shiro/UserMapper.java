package com.jk.mapper.shiro;

import com.jk.model.User;
import com.jk.model.tree.BootTree;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserMapper {


    User selectbyname(String userName);

    User queryname(String userName);

    List<String> puerybyid(Integer userId);

    List<BootTree> querytree(@Param("id") Integer id);
}
