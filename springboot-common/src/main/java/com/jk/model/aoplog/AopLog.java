package com.jk.model.aoplog;

import lombok.Data;

@Data
public class AopLog {
    private String url;//请求路径
    private String method;//请求的方法
    private String ip;//ip
    private String classMethod;//类方法
    private String args;//参数
    private String response;//返回参数

}
