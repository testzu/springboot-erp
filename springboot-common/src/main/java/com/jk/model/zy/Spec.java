package com.jk.model.zy;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class Spec implements Serializable {

    private Integer specid;

    private String  specname;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date specdate;

}
