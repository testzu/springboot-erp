package com.jk.controller.tree;

import com.alibaba.dubbo.common.json.JSONArray;
import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.tree.BootTree;
import com.jk.model.tree.ClassTree;
import com.jk.model.tree.TreeBean;
import com.jk.model.tree.Ztree;
import com.jk.service.tree.BootService;
import netscape.security.Privilege;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.reflect.generics.tree.Tree;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("boot")
public class BootController {

    @Reference
    private BootService bootService;


    /**
     * 跳转index 主页面
     * @return
     */
    @RequestMapping("toIndex")
    public String toIndex(){
        return "tree/index";
    }
    /**
     * index页面下拉树
     * @return
     */
    @RequestMapping("getBootTree")
    @ResponseBody
    public List<BootTree> getBootTree(){
        List<BootTree> bootTreeList = bootService.getBootTree();
        return bootTreeList;
    }


    /**
     * Ztree跳转页面
     * @return
     */
    @RequestMapping("tozTree")
    public String tozTree() {
        return "tree/classTree2";

    }

    /**
     * Ztree展示页面
     * @return
     */
    @RequestMapping("queryZtreeList")
    @ResponseBody
    public List<Ztree> queryZtreeList(){

        List<Ztree> list = bootService.queryZtreeList();
        return list;
    }

    /**
     * 跳转index2页面
     * @return
     */
    @RequestMapping("toIndex2")
    public String toIndex2(){
        return "tree/index2";
    }

    /**
     * 跳转index3 easyuiTree页面
     * @return
     */
    @RequestMapping("toIndex3")
    public String toIndex3(){
        return "tree/index3";
    }

    /**
     * 商品类型树 index3 easyuiTree
     * @return
     */
    @RequestMapping("geteasyuiTree")
    @ResponseBody
    public List<TreeBean> geteasyuiTree(){
        List<TreeBean> TreeList = bootService.geteasyuiTree();
        return TreeList;
    }

    /**
     * 跳转到商品类型树 bootStrapTree
     */
    @RequestMapping("toClassTree")
    public String toClassTree(){
        return "tree/classTree";
    }
    /**
     * 商品类型树 bootStrapTree
     * @return
     */
    @RequestMapping("getClassTree")
    @ResponseBody
    public List<ClassTree> getClassTree(){
        List<ClassTree> classTreeList = bootService.getClassTree();
        return classTreeList;
    }

    /**
     * 创建根分类
     * @return
     */

    @RequestMapping("toClassAdd")

    public String toAdd(){
        return "tree/classAdd";
    }



    /**
     * 添加ztree
     * @param ztree
     * @return
     */

    @RequestMapping("savePidClass")
    public String savePidClass(Ztree ztree){
         bootService.savePidClass(ztree);
         return "redirect:tozTree";
    }







    /**
     * 插件树
     * @param ztree
     * @return
     */
    @RequestMapping("addEasyuiTree")
    public String addEasyuiTree(TreeBean treeBean){
        bootService.addEasyuiTree(treeBean);
        return "redirect:toClassAdd";
    }

    /**
     * 跳转插件树页面
     * @param ztree
     * @return
     */
    @RequestMapping("toTreeBeanUtils")
    public String toTreeBeanUtils() {
        return "tree/classAdd";
    }


    @RequestMapping("getTreeBeanUtils")
    @ResponseBody
    public List<TreeBean> getTreeBeanUtils(){
        List<TreeBean> treeBeanList = bootService.getTreeBeanUtils();
        return treeBeanList;
    }

    /**
     * 树节点新增+修改
     */
    @RequestMapping("saveTreeBean")
    @ResponseBody
    public Boolean save(TreeBean treeBean) {
		/*String encodePassWord = MD5Util.MD5Encode(empBean.getPassword(), "utf-8");
		empBean.setPassword(encodePassWord);*/
        try {
            //有id走修改  没有id走新增
            if(treeBean.getId() !=null) {
                bootService.updateTreeBean(treeBean);
            }else {
                bootService.saveTreeBean(treeBean);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 树节点回显
     */
    @RequestMapping("queryById")
    @ResponseBody
    public TreeBean queryById(TreeBean treeBean) {
        TreeBean queryById = bootService.queryById(treeBean);
        return queryById;
    }

    /**
     * 树节点删除
     */
    @RequestMapping("delByIds")
    @ResponseBody
    public Boolean delByIds(Integer id) {
        try {
            bootService.delByIds(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
