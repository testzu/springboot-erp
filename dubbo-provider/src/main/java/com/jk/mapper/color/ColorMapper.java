package com.jk.mapper.color;

import com.jk.model.color.Color;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ColorMapper {

    List<Color> querycolor(Color color);

    void savecolor(Color color);

    void deletecolor(@Param("id") Integer id);

    Color findUserById(Integer id);

    void editcolor(Color color);

    int queryMaxSort();
}
