package com.jk.service.tree;

import com.jk.model.tree.BootTree;
import com.jk.model.tree.ClassTree;
import com.jk.model.tree.TreeBean;
import com.jk.model.tree.Ztree;

import java.util.List;

public interface BootService {

    List<BootTree> getBootTree();

    List<ClassTree> getClassTree();

    List<Ztree> queryZtreeList();

    List<TreeBean> geteasyuiTree();

    void savePidClass(Ztree ztree);


    void addEasyuiTree(TreeBean treeBean);

    List<TreeBean> getTreeBeanUtils();

    void saveTreeBean(TreeBean treeBean);

    void updateTreeBean(TreeBean treeBean);

    TreeBean queryById(TreeBean treeBean);

    void delByIds(Integer id);

}
