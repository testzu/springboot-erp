/** 
 * <pre>项目名称:ZTree 
 * 文件名称:Ztree.java 
 * 包名:com.jk.tly.tree.model 
 * 创建日期:2018年11月28日下午8:41:49 
 * Copyright (c) 2018, lxm_man@163.com All Rights Reserved.</pre> 
 */  
package com.jk.model.tree;

import lombok.Data;

import java.io.Serializable;

@Data
public class Ztree implements Serializable {
	private Integer id;
	private String name;
	private Integer pid;
	private Boolean open =true;
	private String file;
	/*private String icon;//图标样式
	private String font;//字体State*/
	private String state;
}
