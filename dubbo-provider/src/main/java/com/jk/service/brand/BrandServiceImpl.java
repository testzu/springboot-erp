package com.jk.service.brand;


import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.brand.BrandMapper;
import com.jk.model.brand.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Service(interfaceClass = BrandService.class)
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandMapper brandMapper;


    @Override
    public List<Brand> queryBrandList(Brand branch) {

        List<Brand> list = brandMapper.queryBrandList(branch);

        return list;
    }

    @Override
    public void deleteBrandById(String[] ids) {
        brandMapper.deleteBrandById(ids);
    }

    @Override
    public void updateBrand(Brand branch) {
        brandMapper.updateBrand(branch);
    }

    @Override
    public void addBrand(Brand branch) {
        brandMapper.addBrand(branch);
    }

    @Override
    public Brand toUpdateBrand(Brand brand) {
        return brandMapper.toUpdateBrand(brand);
    }

}
