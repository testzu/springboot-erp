package com.jk.service.color;

import com.jk.model.color.Color;

import java.util.List;

public interface ColorService {


    List<Color> querycolor(Color color);

    void savecolor(Color color);

    void deletecolor(Integer id);

    Color findUserById(Integer id);

    void editcolor(Color color);
}
