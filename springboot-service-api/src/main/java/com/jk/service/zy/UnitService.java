package com.jk.service.zy;

import com.jk.model.zy.Unit;

import java.util.HashMap;
import java.util.List;


public interface UnitService {

    void addunit(Unit unit);

    void deleteunitbyid(Integer id);


    HashMap<String, Object> queryunit(Integer pageSize, Integer start, Unit unit);
}
