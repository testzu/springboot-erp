package com.jk.service.commodity;

import com.jk.model.commodity.Commodity;

import java.util.List;

public interface CommodityService {

    List<Commodity> selectCommodityList(Commodity comm);

    Commodity queryCommodityById(Integer id);
}
