package com.jk.service.user;

import com.jk.model.User;
import com.jk.model.tree.BootTree;

import java.util.List;


public interface UserService {

    User selectbyname(String userName);

    User queryname(String userName);

    List<String> puerybyid(Integer userId);

    List<BootTree> querytree( );
}
