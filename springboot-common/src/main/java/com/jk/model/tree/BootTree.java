package com.jk.model.tree;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BootTree implements Serializable {
    private Integer id;

    private String text;

    private String href;

    private Boolean selectable;

    private List<BootTree> nodes;

    private Integer pid;
}
