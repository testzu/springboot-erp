package com.jk.model.zy;

import lombok.Data;

import java.io.Serializable;

@Data
public class Unit implements Serializable {

    private Integer unitid;

    private String  unitname;

    private String  unitrank;

    private String unitremark;

}
