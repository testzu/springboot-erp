package com.jk.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.model.tree.BootTree;
import com.jk.service.user.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
@Service(interfaceClass = TestService.class)
public class TestServiceImpl implements TestService {
    //test mongoTemplate
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public void testmongdb(){
        BootTree bootTree = new BootTree();
        bootTree.setHref("11111.do");
        bootTree.setText("成功了把");
        bootTree.setId(222222);
        mongoTemplate.save(bootTree);
    }
    @Override
    public String testont() {
        return "连接通了";
    }


}
