package com.jk.mapper.zy;

import com.jk.model.zy.Spec;
import com.jk.model.zy.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SepcMapper {

    Integer queryspecCount(@Param("spec") Spec spec);

    List<Unit> queryepeclist(@Param("pageSize")Integer pageSize, @Param("start")Integer start, @Param("spec")Spec spec);

    void addspec(@Param("spec") Spec spec);

    Spec queryspecById(@Param("id") Integer id);

    void updatespec(@Param("spec") Spec spec);

    void deletespecbyid(@Param("id") Integer id);
}
