package com.jk.controller.zy;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.zy.Unit;
import com.jk.service.zy.UnitService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;


@Controller
@RequestMapping("unit")
public class UnitController {

    @Reference
    private UnitService unitService;


    @RequestMapping("togoodsunir")
    public String toindex() {
        return "zy/unit/selectunit";
    }

    /**
     * 查询商品单位 并跳转goodsunit页面
     * @param
     * @return
     */
    @RequestMapping("queryunit")
    @ResponseBody
    public HashMap<String, Object> queryunit(Integer pageSize,Integer start, Unit unit){
        HashMap<String, Object> map=unitService.queryunit(pageSize,start,unit);
        return map;
    }

    /**
     * 跳转新增页面
     * @return
     */
    @RequestMapping("goodsinset")
    public ModelAndView goodsinset() {

        ModelAndView mav = new ModelAndView();

        mav.setViewName("zy/unit/goodsinset");

        return mav;
    }

    /**
     * 商品单位增加
     * @param unit
     */
    @RequestMapping("addunit")
    @ResponseBody
    public void addunit(Unit unit){
        unitService.addunit(unit);
    }

    /**
     * 商品单位 删除并重定向
     * @param id
     * @return
     */
    @RequestMapping("deleteunitbyid")
    @ResponseBody
    public void deleteunitbyid(Integer id) {
        unitService.deleteunitbyid(id);
    }

}
