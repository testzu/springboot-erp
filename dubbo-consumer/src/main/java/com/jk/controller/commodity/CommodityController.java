package com.jk.controller.commodity;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.commodity.Commodity;
import com.jk.service.commodity.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;

@Controller
public class CommodityController {
    @Reference
    private CommodityService commodityService;

    @RequestMapping("toCommodity")
    public String toCommodity(){
        return "commodity/man";
    }
//    @RequestMapping("selectCommodityList")
//    @ResponseBody
//    public HashMap<String,Object> selectCommodityList(Integer pageSize, Integer start, String brandName){
//    List<Commodity> list= commodityService.selectCommodityList();
//        HashMap<String,Object> hashMap = new HashMap<String,Object>();
//        hashMap.put("rows ",list);
//        hashMap.put("total ",1);
//        return hashMap;
//}
    @RequestMapping("selectCommodityList")
    @ResponseBody
    public List<Commodity> selectCommodityList(Integer pageSize, Integer start, Commodity comm){
     List<Commodity> list= commodityService.selectCommodityList(comm);
        return list;
 }
    @RequestMapping("showImg")
    public ModelAndView showImg(String aa){

        ModelAndView mav = new ModelAndView();
        mav.setViewName("commodity/showImg");
        mav.addObject("img", aa);
        return mav;
    }
    @RequestMapping("queryCommodityById")
    public ModelAndView queryCommodityById(Integer id){
        Commodity commodity= commodityService.queryCommodityById(id);
        ModelAndView mav = new ModelAndView();
        mav.setViewName("commodity/updateCommodity");
        mav.addObject("commodity", commodity);
        return mav;
    }

}
