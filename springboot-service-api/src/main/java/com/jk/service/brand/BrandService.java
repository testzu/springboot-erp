package com.jk.service.brand;

import com.jk.model.brand.Brand;

import java.util.List;


public interface BrandService {

    List<Brand> queryBrandList(Brand branch);

    void deleteBrandById(String[] ids);

    void updateBrand(Brand branch);

    void addBrand(Brand branch);

    Brand toUpdateBrand(Brand brand);
}
