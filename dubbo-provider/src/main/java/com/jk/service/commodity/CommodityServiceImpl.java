package com.jk.service.commodity;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.commodity.CommodityMapper;
import com.jk.model.commodity.Commodity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = CommodityService.class)
@Component
public class CommodityServiceImpl implements CommodityService {
    @Autowired
    private CommodityMapper commodityMapper;

    @Override
    public List<Commodity> selectCommodityList(Commodity comm) {
        return commodityMapper.selectCommodityList(comm);
    }

    @Override
    public Commodity queryCommodityById(Integer id) {
        return commodityMapper.queryCommodityById(id);
    }

}
