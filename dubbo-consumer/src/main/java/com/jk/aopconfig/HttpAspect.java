package com.jk.aopconfig;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.aoplog.AopLog;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class HttpAspect {
    AopLog aopLog= new AopLog();
    @Autowired
    private MongoTemplate mongoTemplate;
    //@Pointcut("execution(com.jk.controller.*(..))")
    @Pointcut("execution(* com.jk.controller.*..*.*(..))")
    public void pointcut(){}
    @Before("pointcut()")
    public void doBefore (JoinPoint joinPoint){
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
       // System.out.println("url"+request.getRequestURL());
        aopLog.setUrl(""+request.getRequestURL());
        //System.out.println("method"+request.getMethod());
        aopLog.setMethod(request.getMethod());
        //System.out.println("ip"+request.getRemoteAddr());
        aopLog.setIp(request.getRemoteAddr());
       // System.out.println("classmethod"+joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName());
        aopLog.setClassMethod(joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName());
       // System.out.println("args"+joinPoint.getArgs());
        aopLog.setArgs(""+joinPoint.getArgs());
    }
    @AfterReturning(returning = "object",pointcut = "pointcut()")
    public void doAfterrutnning(Object object){
     //   System.out.println("response"+object.toString());
        if(object!=null){
            aopLog.setResponse(object.toString());
        }
       // System.out.println(aopLog);
        mongoTemplate.save(aopLog);
    }
}
