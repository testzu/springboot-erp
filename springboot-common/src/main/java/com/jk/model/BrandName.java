package com.jk.model;


import lombok.Data;

import java.io.Serializable;

@Data
public class BrandName implements Serializable {

    private Integer brandId;// id
    private Integer  brandNo;//编号
    private String  brandName;//名称
    private Integer  brId;//品牌id
    private Integer brandType;//分类id
    private String brandState;//状态
    private String brandCover;//封面
    private String brandVideo;//视频
    private String text;// 分类名程
    private  String brand;
    private  String brandDescribe;//描述
    private  String brandNote;//备注
    private Integer brandOrdinarytax;//普通税
    private Integer  brandTheVAT;//增值税









}
