package com.jk.service.zy;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.zy.UnitMapper;
import com.jk.model.zy.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;


@Service(interfaceClass = UnitService.class )
@Component
public class UnitServiceImpl implements UnitService{

    @Autowired
    private UnitMapper unitMapper;

    @Override
    public void addunit(Unit unit) {
        unitMapper.addunit(unit);
    }

    @Override
    public void deleteunitbyid(Integer id) {
        unitMapper.deleteunitbyid(id);
    }

    @Override
    public HashMap<String, Object> queryunit(Integer pageSize, Integer start, Unit unit) {
        Integer unitCount = unitMapper.queryunitCount(unit);

        List<Unit> unitList = unitMapper.queryunitlist(pageSize,start,unit);

        HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("rows", unitList);

        map.put("total",unitCount);

        return map;
    }


}
