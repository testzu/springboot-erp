package com.jk.controller.brand;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.brand.Brand;
import com.jk.service.brand.BrandService;
import com.jk.utils.ChineseCharToEnUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;


@Controller
@RequestMapping("brand")
public class BrandController {

    @Reference
    private BrandService brandService;

    @RequestMapping("toBrand")
    public String toFood(){

        return "view/brandList";
    }
    //回显
    @RequestMapping("toupdBrand")
    public String toupdBrand(Brand brand, ModelMap modelMap){
        Brand brand1 = brandService.toUpdateBrand(brand);
        modelMap.addAttribute("listU",brand1);
        return "view/udateBrand";
    }

    @RequestMapping("toAddBrand")
    public String toAddUser(){

        return "view/addBrand";
    }


    //查询
    @RequestMapping("queryBrandList")
    @ResponseBody
    public List<Brand> queryBrandList(Brand branch){
        List<Brand> list = brandService.queryBrandList(branch);
        return list;
    }


    //新增
    @RequestMapping("addBrand")
    public String addBrand(Brand branch,HttpServletRequest request){

        if (branch.getBrandId()==null){

            String brandCover = branch.getBrandCover();
            String path = "http://"+request.getServerName()+":"+
                    request.getServerPort()+request.getContextPath()+"/"+brandCover;
            branch.setBrandCover(path);

            String brandMovie = branch.getBrandMovie();
            String path1 = "http://"+request.getServerName()+":"+
                    request.getServerPort()+request.getContextPath()+"/"+brandMovie;
            branch.setBrandMovie(path1);

            String name = branch.getBrandName();
            ChineseCharToEnUtil chineseCharToEnUtil = new ChineseCharToEnUtil();
            String firstSpell = chineseCharToEnUtil.getFirstSpell(name);
            String substring = firstSpell.substring(0, 1);

            branch.setBrandFirst(substring);

            brandService.addBrand(branch);

        }else{

            String brandCover = branch.getBrandCover();
            String path = "http://"+request.getServerName()+":"+
                    request.getServerPort()+request.getContextPath()+"/"+brandCover;
            branch.setBrandCover(path);

            String brandMovie = branch.getBrandMovie();
            String path1 = "http://"+request.getServerName()+":"+
                    request.getServerPort()+request.getContextPath()+"/"+brandMovie;
            branch.setBrandMovie(path1);
            brandService.updateBrand(branch);

        }
        return "redirect:/brand/toBrand";
    }

    //批删
    @RequestMapping("deleteBrandById")
    @ResponseBody
    public Boolean deleteBrandById(String[] ids){
        try {
            brandService.deleteBrandById(ids);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping("upload")
    @ResponseBody
    public static HashMap<String, String> saveImg(MultipartFile brandCover,HttpServletRequest request) throws IOException {
        HashMap<String, String> result = new HashMap<>();
        String path = request.getRealPath("/");
        //String url ="photo";
       //String path = request.getSession().getServletContext().getRealPath(url);
       // String path = "D:\\1806x\\springboot-erp\\dubbo-consumer\\src\\main\\resources\\photo";
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        FileInputStream fileInputStream = (FileInputStream) brandCover.getInputStream();
        String fileName = UUID.randomUUID() + ".png";
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path + File.separator + fileName));
        byte[] bs = new byte[1024];
        int len;
        while ((len = fileInputStream.read(bs)) != -1) {
            bos.write(bs, 0, len);
        }
        bos.flush();
        bos.close();
        result.put("img", fileName);
        return result;
    }


    @RequestMapping("upload2")
    @ResponseBody
    public static HashMap<String, String> saveImg2(MultipartFile brandMovie,HttpServletRequest request) throws IOException {
        HashMap<String, String> result = new HashMap<>();
        String path = request.getRealPath("/");
        //String url ="photo";
        // String path = request.getSession().getServletContext().getRealPath(url);
        // String path = "D:\\1806x\\springboot-erp\\dubbo-consumer\\src\\main\\resources\\photo";
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        FileInputStream fileInputStream = (FileInputStream) brandMovie.getInputStream();
        String fileName = UUID.randomUUID() + ".mp4";
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path + File.separator + fileName));
        byte[] bs = new byte[1024];
        int len;
        while ((len = fileInputStream.read(bs)) != -1) {
            bos.write(bs, 0, len);
        }
        bos.flush();
        bos.close();
        result.put("move", fileName);
        return result;
    }
}
