package com.jk.mapper.tree;

import com.jk.model.tree.BootTree;
import com.jk.model.tree.ClassTree;
import com.jk.model.tree.TreeBean;
import com.jk.model.tree.Ztree;

import java.util.List;

public interface BootMapper {
    List<BootTree> getBootTreeNodes(Integer pid);

    List<ClassTree> getClassTreeNodes(Integer pid);

    List<Ztree> queryZtreeList();

    List<TreeBean> queryTreeNode(Integer id);

    void savePidClass(Ztree ztree);

    void addEasyuiTree(TreeBean treeBean);

    List<TreeBean> getTreeBeanUtils();

    void saveTreeBean(TreeBean treeBean);

    void updateTreeBean(TreeBean treeBean);

    TreeBean queryById(TreeBean treeBean);

    void delByIds(Integer id);

}
