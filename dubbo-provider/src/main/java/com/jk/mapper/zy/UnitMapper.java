package com.jk.mapper.zy;

import com.jk.model.zy.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UnitMapper {

    void addunit(@Param("unit") Unit unit);

    void deleteunitbyid(@Param("id") Integer id);

    Integer queryunitCount(@Param("unit") Unit unit);

    List<Unit> queryunitlist(@Param("pageSize")Integer pageSize, @Param("start")Integer start, @Param("unit")Unit unit);
}
