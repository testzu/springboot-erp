package com.jk.service.color;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.color.ColorMapper;
import com.jk.model.color.Color;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = ColorService.class)
@Component
public class ColorServiceImpl implements ColorService {

    @Autowired
    private ColorMapper colorMapper;

    @Override
    public List<Color> querycolor(Color color) {
        return colorMapper.querycolor(color);
    }

    @Override
    public void savecolor(Color color) {
        int max=colorMapper.queryMaxSort();
        color.setColorSort(max+1);
        colorMapper.savecolor(color);
    }

    @Override
    public void deletecolor(Integer id) {
        colorMapper.deletecolor(id);
    }

    @Override
    public Color findUserById(Integer id) {
        Color color=colorMapper.findUserById(id);
        return color;
    }

    @Override
    public void editcolor(Color color) {
        colorMapper.editcolor(color);
    }
}
