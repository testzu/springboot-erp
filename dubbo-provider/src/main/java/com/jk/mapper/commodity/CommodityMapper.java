package com.jk.mapper.commodity;

import com.jk.model.commodity.Commodity;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CommodityMapper {
    List<Commodity> selectCommodityList(Commodity comm);

    Commodity queryCommodityById(Integer id);
}
