package com.jk.model.brand;


import lombok.Data;

import java.io.Serializable;

@Data
public class Brand implements Serializable {

    private Integer brandId;
    private String brandNumber;
    private String brandFirst;
    private String brandName;
    private String brandCover;
    private String brandMovie;

}
