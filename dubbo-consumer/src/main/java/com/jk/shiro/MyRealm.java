package com.jk.shiro;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.User;
import com.jk.service.user.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class MyRealm extends AuthorizingRealm {
    @Reference
    private UserService userService;

    @Bean(name = "userService")
    public UserService getUserService(){
        return userService;
    }

    @Override//认证方法
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String userName = (String)token.getPrincipal();//获取输入框里的用户名
        userService=SpringBeanFactoryUtils.getBean("userService", UserService.class);
        User user = userService.queryname(userName);
        //如果根据用户名查询用户对象 返回值为null 则证明 用户名不存在
        if(user == null){
            //抛出用户名未知异常
            throw new UnknownAccountException();
        }
        //new 一个简单认证器 第一个参数为用户对象 第二个参数为用户密码 第三个参数为当前realm名称
        //第一个参数相当于 session.setAttribute("user",user);
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(user,user.getUserPassword(),this.getName());
        //返回认证结果
        return simpleAuthenticationInfo;
    }


    @Override//授权方法
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

}
