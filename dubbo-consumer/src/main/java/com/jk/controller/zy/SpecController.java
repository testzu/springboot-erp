package com.jk.controller.zy;
import com.jk.model.zy.Spec;
import com.jk.model.zy.Unit;
import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.service.zy.SepcService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@Controller
@RequestMapping("spec")
public class SpecController {

    @Reference
    private SepcService sepcService;

    /**
     * 跳转展示页面
     * @return
     */
    @RequestMapping("togoodsSepc")
    public String togoodsSepc() {
        return "zy/sepc/goodsSepc";
    }

    /**
     * 规格查询（带分页）
     * @param pageSize
     * @param start
     * @param spec
     * @return
     */
    @RequestMapping("querysepc")
    @ResponseBody
    public HashMap<String, Object> querysepc(Integer pageSize, Integer start, Spec spec){
        HashMap<String, Object> map=sepcService.querysepc(pageSize,start,spec);
        return map;
    }

    /**
     * 跳转到新增弹框页面
     * @return
     */
    @RequestMapping("toaddgoods")
    public ModelAndView toaddgoods() {

        ModelAndView mav = new ModelAndView();

        mav.setViewName("zy/sepc/addspec");

        return mav;
    }

    /**
     * 新增
     * @param spec
     */
    @RequestMapping("addspec")
    @ResponseBody
    public void addspec(Spec spec){
        sepcService.addspec(spec);
    }

    /**
     * 修改-根据id回显
     * @param id
     * @return
     */
    @RequestMapping("queryspecById")
    public ModelAndView queryspecById(Integer id){

        ModelAndView mav = new ModelAndView();

        Spec spec=sepcService.queryspecById(id);
        mav.setViewName("zy/sepc/updatespec");

        mav.addObject("spec", spec);

        return mav;
    }

    /**
     * 修改
     * @param spec
     */
    @RequestMapping("updatespec")
    @ResponseBody
    public void updatespec(Spec spec){
        sepcService.updatespec(spec);
    }

    /**
     * 删除
     * @param id
     */
    @RequestMapping("deletespecbyid")
    @ResponseBody
    public void deletespecbyid(Integer id){
        sepcService.deletespecbyid(id);
    }



}
