package com.jk.service.brand;


import com.jk.model.Brand;
import com.jk.model.BrandName;
import com.jk.model.ClassTree;


import java.util.HashMap;
import java.util.List;

public interface BrandNameService {


    void gettest();


    HashMap<String, Object> selectBrandNameList(Integer pageSize, Integer start, BrandName brandName);

    List<Brand> queryBrandNameType();

    List<Brand> queryBrandNameTree();

    List<Brand> queryBrandList();

    List<ClassTree> queryClassTree();

    void saveBrandName(BrandName BrandName);
}
