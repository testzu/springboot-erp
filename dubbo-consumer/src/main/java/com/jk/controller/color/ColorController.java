package com.jk.controller.color;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.color.Color;
import com.jk.service.color.ColorService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
@RequestMapping("color")
public class ColorController {

    @Reference
    private ColorService colorService;

    @RequestMapping("tocolor")
    public String tocolor() {
        return "color/colorlist";
    }

    //查询
    @RequestMapping("querycolor")
    @ResponseBody
    public List<Color> querycolor(Color color){
        return colorService.querycolor(color);
    }
/*
    @RequestMapping("toindex")
    public String toindex() {
        return "index";
    }*/

    @RequestMapping("toAdd")
    public String toAdd() {
        return "color/addColor";
    }

    //新增
    @RequestMapping("savecolor")
    @ResponseBody
    public String savecolor(Color color) {
        colorService.savecolor(color);
        return "1";
    }

    //删除
    @RequestMapping("deletecolor")
    @ResponseBody
    public void deletecolor(Integer id) {
        colorService.deletecolor(id);
    }

    //回显
    @RequestMapping("toEditcolor")
    public ModelAndView findUserById(Integer id) {
        ModelAndView mav=new ModelAndView();
        Color color=colorService.findUserById(id);
        mav.addObject("color",color);
        mav.setViewName("color/userEdit");
        return mav;
    }

    //修改
    @RequestMapping("editcolor")
    @ResponseBody
    public String editcolor(Color color) {
        colorService.editcolor(color);
        return "1";
    }
}
