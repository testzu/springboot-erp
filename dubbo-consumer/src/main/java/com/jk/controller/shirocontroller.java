package com.jk.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.tree.BootTree;
import com.jk.service.user.UserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class shirocontroller {

     @Reference
    private UserService userService;

    @RequestMapping("/login")
    public String  login(HttpServletRequest HttpServletRequest, ModelMap ModelMap){
        //获取到shiro认证失败之后异常类的类名不知道哪里来的
        String exceptionClassName = (String) HttpServletRequest.getAttribute("shiroLoginFailure");
        //UnknownAccountException 异常类MyRealm查询的对象为空的异常
        if (UnknownAccountException.class.getName().equals(exceptionClassName)|| AuthenticationException.class.getName().equals(exceptionClassName)){
            ModelMap.put("status","{用户名异常}");
        }else{
            ModelMap.put("status","{用户密码异常}");
        }

        return "toMain";
    }


    @RequestMapping("toLogin")
    public String  toLogin(){

        return "login";
    }

    @RequestMapping("queryadd")
    @RequiresPermissions("shiro:add")
    public String queryadd(){
        System.out.println("权限成功了");
        return "/add";
    }


    @RequestMapping("querytree")
    @RequiresPermissions("shiro:querytree")
    @ResponseBody
    public List<BootTree> querytree( ){

        List<BootTree> list= userService.querytree();
        return list;
    }

    @RequestMapping("toMain")
    public String toMain(){

        return "/toMain";
    }


}
