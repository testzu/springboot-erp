package com.jk.model.color;

import lombok.Data;

import java.io.Serializable;

@Data
public class Color implements Serializable {

    private Integer colorId;
    private String colorName;
    private Integer colorSort;
    private String  colorRemark;


}
