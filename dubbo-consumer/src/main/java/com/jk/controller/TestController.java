package com.jk.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.service.user.TestService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {

    @Reference
    private TestService testService;

    @ResponseBody
    @RequestMapping("testOnt")
    public String testont(){
       String test= testService.testont();
        return "你好"+test;
    }
    @RequestMapping("toIndex")
    public String toIndex(){
        return "index2";
    }
    @RequestMapping("testmongdb")
    public void testmongdb(){
        testService.testmongdb();
    }
}
