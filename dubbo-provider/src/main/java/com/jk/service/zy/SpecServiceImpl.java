package com.jk.service.zy;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.zy.SepcMapper;
import com.jk.model.zy.Spec;
import com.jk.model.zy.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service(interfaceClass = SepcService.class)
@Component
public class SpecServiceImpl implements SepcService{

    @Autowired
    private SepcMapper sepcMapper;

    @Override
    public HashMap<String, Object> querysepc(Integer pageSize, Integer start, Spec spec) {
        Integer specCount = sepcMapper.queryspecCount(spec);

        List<Unit> specList = sepcMapper.queryepeclist(pageSize,start,spec);

        HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("rows", specList);

        map.put("total",specCount);

        return map;
    }

    @Override
    public void addspec(Spec spec) {
        spec.setSpecdate(new Date());
        sepcMapper.addspec(spec);
    }

    @Override
    public Spec queryspecById(Integer id) {
        return sepcMapper.queryspecById(id);
    }

    @Override
    public void updatespec(Spec spec) {
        sepcMapper.updatespec(spec);
    }

    @Override
    public void deletespecbyid(Integer id) {
        sepcMapper.deletespecbyid(id);
    }
}
